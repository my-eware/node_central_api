from tools.setup import Resource, ssh_exception, request
from tools.ssh import RemoteServer
from tools.security import token_required

class ToDicts():
    @classmethod    
    def disk_dict(cls,output):
        lines = output.readlines()
        lines_list =  []
        info_dic = {
            "size":"",
            "used":"",
            "available":"",
            "used_p":"",
            "location":""
        }
        final_dic = {}

        for line in lines:
            lines_list.append(line.strip().split())

        for i in range(1,len(lines_list)):
            counter = 1
            for key in info_dic.keys():
                info_dic.update({key:lines_list[i][counter]})
                counter+=1
            final_dic.update({lines_list[i][0]: info_dic})

        return final_dic

    @classmethod
    def top_processes_dict(cls,output):
        lines = output.readlines()
        lines_list =  []
        info_dic = {
                "user":"",
                "pr":"",
                "ni":"",
                "virtual_mem":"",
                "res":"",
                "shr":"",
                "status":"",
                "cpu_use_p":"",
                "mem_use_p":"",
                "time":"",
                "command":""
            }
        final_dic = {}

        for line in lines:
            lines_list.append(line.strip().split())

        for i in range(1,len(lines_list)):
            counter = 1
            for key in info_dic.keys():
                info_dic.update({key:lines_list[i][counter]})
                counter+=1
            final_dic.update({lines_list[i][0]:dict(info_dic)})
        
        return final_dic

    @classmethod
    def mem_dict(cls,output):
        lines = output.readlines()
        filtered_string = ""

        info_dic = {
            "total":"",
            "free":"",
            "used":"",
            "buff/cache":""
        }

        final_dic = {}

        for x in lines[0]:
            if (x!="," and x!=":" and x!="\n"):
                filtered_string = filtered_string + x

        output_list = []
        output_list.append(filtered_string.strip().split("   ")[0][0:3])

        for x in filtered_string.strip().split("   ")[1:]:
            for i in x.split(" "):
                try:
                    output_list.append(float(i))
                except:
                    continue

        counter = 1
        for key in info_dic.keys():
            info_dic.update({key:output_list[counter]})
            counter += 1

        final_dic.update({output_list[0]:info_dic})

        return final_dic

class ServerConsult(Resource):
    @token_required
    def post(self, option):
        if option not in ("disk_status", "memory_status", "top_processes_status"):
            return {"message" : "Route not found"}, 404

        try:
            body = request.json
            server = RemoteServer(username = body["username"], 
                                passwd = body["password"], 
                                hostname = body["ip"], 
                                port = body["port"])
            if option == "disk_status":
                output_info = ToDicts.disk_dict(server.getDiskStatus())
            
            elif option == "memory_status":
                output_info = ToDicts.mem_dict(server.getMemStatus())
            
            elif option == "top_processes_status":
                output_info = ToDicts.top_processes_dict(server.getTopThreeAppsStatus())
            
            server.closeConnection()
            return output_info, 200
        except ssh_exception.SSHException:
            return {"message" : "Remote server interrupted connection or bad input"}, 400
        except AttributeError:
            return {"message" : "Couldn't connect to remote server"}, 400
        except Exception as e:
            return {"message": e.args[1]}, 400