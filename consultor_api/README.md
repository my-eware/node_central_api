## Python Setup environment

MacOS or Linux

```bash
$ virtualenv --python=3.9 venv
$ . venv/bin/activate
$ pip install -r venv/requirements.txt
```

## Consult

Via `test.py`

```terminal
python3 app.py
python3 test.py
```