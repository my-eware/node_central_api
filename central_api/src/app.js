import "dotenv/config";
import Joi from "joi";
import jwt from "jsonwebtoken";
import express, { response } from "express";
const app = express();

// Tokens, RegEx, etc.
import config from "../../config.json" assert { type: "json" };

// Middleware
app.use(express.json());

app.post(
  "/api/consult/memory_status",
  authenticateToken,
  authenticateBody,
  async (req, res) => {
    try {
      const data = await consult_API(
        "http://localhost:5000/server/memory_status",
        req.body
      );
      res.status(data.status).json(data.message);
    } catch (error) {
      res.status(400).json({ message: "API not available" });
    }
  }
);

app.post(
  "/api/consult/disk_status",
  authenticateToken,
  authenticateBody,
  async (req, res) => {
    try {
      const data = await consult_API(
        "http://localhost:5000/server/disk_status",
        req.body
      );
      res.status(data.status).json(data.message);
    } catch (error) {
      res.status(400).json({ message: "API not available" });
    }
  }
);

app.post(
  "/api/consult/top_processes_status",
  authenticateToken,
  authenticateBody,
  async (req, res) => {
    try {
      const data = await consult_API(
        "http://localhost:5000/server/top_processes_status",
        req.body
      );
      res.status(data.status).json(data.message);
    } catch (error) {
      res.status(400).json({ message: "API not available" });
    }
  }
);

app.post("/login", async (req, res) => {
  const schema = Joi.object()
    .keys({
      username: Joi.string().alphanum().min(6).max(10).required(),
      password: Joi.string().min(8).max(15).required(),
    })
    .with("username", "password");

  // TODO: Autenticar usuario, quizás no está ni sign up.

  try {
    await schema.validateAsync({
      username: req.body.username,
      password: req.body.password,
    });

    const user = { username: req.body.username };
    const accessToken = generateAccessToken(user);
    res.json({ accessToken: accessToken });
  } catch (error) {
    res.status(400).json({ message: error.details[0].message });
  }
});

function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) return res.sendStatus(401);

  jwt.verify(token, config.ACCESS_TOKEN_SECRET, (err, information) => {
    if (err) return res.sendStatus(403);
    // Con esto puedes acceder a la info del webtoken con req.information.username, por ejemplo.
    // Ya que esto es un middleware, en la ruta que yo ponga esta función el req.information será utilizable.
    req.information = information;
    next();
  });
}

function generateAccessToken(user) {
  // return jwt.sign(user, config.ACCESS_TOKEN_SECRET, { expiresIn: "15s"});
  return jwt.sign(user, config.ACCESS_TOKEN_SECRET);
}

function generateAPIToken(info) {
  // return jwt.sign(user, config.ACCESS_TOKEN_SECRET, { expiresIn: "15s"});
  return jwt.sign(info, config.APIS_TOKEN_SECRET, { expiresIn: "15s" });
}

async function authenticateBody(req, res, next) {
  try {
    const schema = Joi.object().keys({
      username: Joi.string().alphanum().max(15).required(),
      password: Joi.string().max(20).required(),
      ip: Joi.string().pattern(new RegExp(config.IP_REGEX)).required(),
      port: Joi.number().integer().min(0).max(65535),
    });
    await schema.validateAsync({
      username: req.body.username,
      password: req.body.password,
      ip: req.body.ip,
      port: req.body.port,
    });
    next();
  } catch (error) {
    res.status(400).json({ message: "Bad input" });
  }
}

async function consult_API(url, req_body) {
  const token = generateAPIToken({ api: "main" });
  const data = await fetch(url, {
    method: "POST",
    headers: {
      authorization: `Bearer ${token}`,
      "content-type": `application/json`,
    },
    body: JSON.stringify(req_body),
  });
  return { status: data.status, message: await data.json() };
}

// PORT
const port = process.env.PORT;

app.listen(port, () => console.log(`Listening on port ${port}...`));
